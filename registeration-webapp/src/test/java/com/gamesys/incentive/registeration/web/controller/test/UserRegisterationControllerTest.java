package com.gamesys.incentive.registeration.web.controller.test;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gamesys.incentive.registeration.web.controller.UserRegisterationController;
import com.gamesys.incentive.registeration.web.data.UserBuilder;
import com.gamesys.incentive.registeration.web.model.User;
import com.gamesys.incentive.registeration.web.service.UserRegisterationService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml"})
public class UserRegisterationControllerTest {

	private MockMvc mockMvc;

	@Autowired
	private ObjectMapper objectMapper;
	
	@Mock
	private UserRegisterationService registerationService;
	
	@InjectMocks
	private UserRegisterationController controller;

	@Before
	public void setup() {
		MockitoAnnotations.initMocks(this);
		this.mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
	}

	@Test
	public void should_register_valid_user() throws Exception {
		User user = new UserBuilder().withUserName("Test1").withPassword("Abcd1").withDob("01/01/1999").withSsn("1234")
				.build();
		
		when(registerationService.userExists(any(User.class))).thenReturn(false);
		when(registerationService.valid(any(User.class))).thenReturn(true);
		
		this.mockMvc.perform(post("/register").contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(objectMapper.writeValueAsString(user)))
		.andDo(MockMvcResultHandlers.print())
		.andExpect(status().isOk());
	}

	@Test
	public void should_not_register_user_with_invalid_password() throws Exception {
		User user = new UserBuilder().withUserName("Test2").withPassword("InvalidPass").withDob("01/01/1999").withSsn("1234")
				.build();
		
		this.mockMvc.perform(post("/register").contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(objectMapper.writeValueAsString(user)))
		.andDo(MockMvcResultHandlers.print())
		.andExpect(status().isBadRequest());
		
		verify(registerationService,never()).userExists(user);
	}
	
	@Test
	public void should_not_register_duplicate_users() throws Exception {
		User user = new UserBuilder().withUserName("Test3").withPassword("Abcd1").withDob("01/01/1999").withSsn("1234")
				.build();
		
		when(registerationService.userExists(any(User.class))).thenReturn(true);
		
		this.mockMvc.perform(post("/register").contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(objectMapper.writeValueAsString(user)))
		.andDo(MockMvcResultHandlers.print())	
		.andExpect(status().isConflict());
		
	}

	@Test
	public void should_not_register_blacklisted_user() throws Exception {
		User user = new UserBuilder().withUserName("Test4").withPassword("Abcd1").withDob("01/01/1999").withSsn("1234")
				.build();
		
		when(registerationService.valid(any(User.class))).thenReturn(false);
		
		this.mockMvc.perform(post("/register").contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(objectMapper.writeValueAsString(user)))
		.andDo(MockMvcResultHandlers.print())
		.andExpect(status().isUnprocessableEntity());
		
	}
}
