package com.gamesys.incentive.registeration.web.data;

import com.gamesys.incentive.registeration.web.model.User;

public class UserBuilder {

	private String userName;
	private String password;
	private String ssn;
	private String dob;
	
	public UserBuilder(){
	}
	
	public UserBuilder withUserName(String userName){
		this.userName = userName;
		return this;
	}
	
	public UserBuilder withPassword(String password){
		this.password = password;
		return this;
	}
	
	public UserBuilder withSsn(String ssn){
		this.ssn= ssn;
		return this;
	}
	
	public UserBuilder withDob(String dob){
		this.dob=dob;
		return this;
	}
	
	public User build(){
		User user = new User();
		user.setUserName(userName);
		user.setPassword(password);
		user.setDob(dob);
		user.setSsn(ssn);
		return user;
	}
}
