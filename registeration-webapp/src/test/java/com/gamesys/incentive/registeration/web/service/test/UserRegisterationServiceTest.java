package com.gamesys.incentive.registeration.web.service.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.gamesys.incentive.registeration.exclusion.ExclusionService;
import com.gamesys.incentive.registeration.web.data.UserBuilder;
import com.gamesys.incentive.registeration.web.model.User;
import com.gamesys.incentive.registeration.web.service.UserRegisterationService;
import com.gamesys.incentive.registeration.web.service.UserRegisterationServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class UserRegisterationServiceTest {

	@Mock
	private ExclusionService exclusionService;
	
	@InjectMocks
	private UserRegisterationService registerationService = new UserRegisterationServiceImpl();
	
	@Test
	public void should_report_user_presence() {
		User user = new UserBuilder().withUserName("Test").build();
		registerationService.registerUser(user);
		assertTrue(registerationService.userExists(user));
	}
	
	@Test
	public void should_call_exclusion_service_with_given_userinfo(){
		User user = new UserBuilder().withDob("01/01/1988").withSsn("1212").build();
		registerationService.valid(user);
		verify(exclusionService,times(1)).validate("01/01/1988", "1212");
	}
	
	@Test
	public void should_validate_user(){
		User user = new UserBuilder().withDob("01/01/1999").withSsn("4321").build();
		when(exclusionService.validate("01/01/1999","4321")).thenReturn(false);
		assertFalse(registerationService.valid(user));
		
		when(exclusionService.validate("01/01/1999","4321")).thenReturn(true);
		assertTrue(registerationService.valid(user));
	}

}
