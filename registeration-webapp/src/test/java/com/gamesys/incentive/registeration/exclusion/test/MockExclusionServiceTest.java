package com.gamesys.incentive.registeration.exclusion.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.gamesys.incentive.registeration.exclusion.MockExclusionServiceImpl;


public class MockExclusionServiceTest {
	
	private MockExclusionServiceImpl exclusionService = new MockExclusionServiceImpl();

	@Test
	public void should_validate_mocked_user() {
		String dob = "01/01/1999";
		String ssn = "1234";
		
		assertTrue(exclusionService.validate(dob, ssn));
		
		exclusionService.addBlackListedUser(dob, ssn);
		assertFalse(exclusionService.validate(dob, ssn));
	}

}
