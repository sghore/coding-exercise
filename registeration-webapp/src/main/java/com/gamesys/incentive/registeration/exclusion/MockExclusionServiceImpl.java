package com.gamesys.incentive.registeration.exclusion;

import java.util.ArrayList;
import java.util.List;

public class MockExclusionServiceImpl implements ExclusionService {

public static final List<String> MOCK_BLACKLIST_USERS = new ArrayList<String>();
	
	public boolean validate(String dob, String ssn) {
		
		return !MOCK_BLACKLIST_USERS.contains(dob+ssn);
	}
	
	public void addBlackListedUser(String dob, String ssn){
		MOCK_BLACKLIST_USERS.add(dob+ssn);
	}

}
