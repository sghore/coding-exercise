package com.gamesys.incentive.registeration.exclusion;

public interface ExclusionService {

	boolean validate(String dob, String ssn);
}
