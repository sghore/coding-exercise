package com.gamesys.incentive.registeration.web.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.gamesys.incentive.registeration.exclusion.ExclusionService;
import com.gamesys.incentive.registeration.web.model.User;

public class UserRegisterationServiceImpl implements UserRegisterationService {

private static final Map<String, User> USER_DATABASE = new HashMap<String, User>();
	
	@Autowired
	private ExclusionService exclusionService;
	
	public boolean userExists(User user) {
		return USER_DATABASE.containsKey(user.getUserName());
	}

	public void registerUser(User user) {
		USER_DATABASE.put(user.getUserName(), user);
	}

	public boolean valid(User user) {
		return exclusionService.validate(user.getDob(), user.getSsn());
	}

	public ExclusionService getExclusionService() {
		return exclusionService;
	}

	public void setExclusionService(ExclusionService exclusionService) {
		this.exclusionService = exclusionService;
	}
}
