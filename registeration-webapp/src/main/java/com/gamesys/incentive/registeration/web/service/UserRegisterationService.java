package com.gamesys.incentive.registeration.web.service;

import com.gamesys.incentive.registeration.web.model.User;

public interface UserRegisterationService {

	boolean userExists(User user);
	
	void registerUser(User user);
	
	boolean valid(User user);
}
