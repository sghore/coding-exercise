package com.gamesys.incentive.registeration.web.model;

import javax.validation.constraints.Pattern;

import com.gamesys.incentive.registeration.web.handler.restriction.PasswordRestriction;


public class User {

	@Pattern(regexp="^[A-Za-z0-9]+$")
	private String userName;
	@PasswordRestriction
	private String password;
	private String ssn;
	private String dob;
	
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getSsn() {
		return ssn;
	}
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	
	
}
