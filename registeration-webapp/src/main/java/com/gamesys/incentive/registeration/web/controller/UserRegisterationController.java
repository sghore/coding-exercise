package com.gamesys.incentive.registeration.web.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.gamesys.incentive.registeration.web.model.User;
import com.gamesys.incentive.registeration.web.service.UserRegisterationService;


@Controller
@RequestMapping("/register")
public class UserRegisterationController {
	
	@Autowired
	private UserRegisterationService registerationService;
	
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<Void> register(@RequestBody @Valid User user){
		if(exists(user)){
			return ResponseEntity.status(HttpStatus.CONFLICT).build();
		}
		
		if(!valid(user)){
			return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).build();
		}
		
		registerationService.registerUser(user);
		return ResponseEntity.status(HttpStatus.OK).build();
	}
	
	private boolean exists(User user){
		return registerationService.userExists(user);
	}
	
	private boolean valid(User user) {
		return registerationService.valid(user);
	}


}
